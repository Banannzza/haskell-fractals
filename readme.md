# Инструкция по сборке

Установка зависимостей
```
cabal instal --only-dependensies
```

Сборка
```
cabal build
```

Запуск исполняемого файла 
```
cabal run
```

# Параметры при запуске
1) Название фрактала : snowflake dragon или curve

2) Номер поколения для генерации

3) Длина линий для отрисовки (0 < lineLength <= 1)

Пример
```
cabal run snowflake 5 0.02
```