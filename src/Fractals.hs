module Fractals where

import Graphics.UI.GLUT


curveRule :: Char -> [Char]
curveRule c
    | c == 'F' = ""
    | c == 'L' = "FL-FR--FR+FL++FLFL+FR-"
    | c == 'R' = "+FL-FRFR--FR-FL++FL+FR"
    | otherwise = [c]


snowflakeRule :: Char -> [Char]
snowflakeRule c
    | c == 'F' = "F-F++F-F" -- для снежинки
    | otherwise = [c]

dragonRule :: Char -> [Char]
dragonRule c
    | c == 'F' = "F" -- для дракона
    | c == 'X' = "X+YF"
    | c == 'Y' = "FX-Y"
    | otherwise = [c]


parseByRule :: (Char -> [Char]) -> [Char] -> [Char]
parseByRule _ [] = []
parseByRule rule inp = parse inp ""
    where parse input result
            | input == [] = result
            | otherwise = parse (tail input) (result ++ rule (head input))



generate :: (Char -> [Char]) -> String -> Int -> [Char]
generate rule aksioma p = generator p  aksioma
    where 
        generator p curentFormula 
            | p == 0 = curentFormula
            | otherwise = generator (p-1) (parseByRule rule curentFormula)




myRotate :: (GLfloat,GLfloat,GLfloat) -> (GLfloat,GLfloat,GLfloat) -> GLfloat -> (GLfloat,GLfloat,GLfloat)
myRotate (x1,y1,z1) (x2,y2,z2) alpha = (x3,y3,z2)
    where
        x3 = x1 + (x2- x1) * cos (alpha * pi / 180) - (y2 - y1) * sin (alpha * pi / 180)
        y3 = y1 + (x2- x1) * sin (alpha * pi / 180) + (y2 - y1) * cos (alpha * pi / 180)

generateLinesPoints :: GLfloat -> GLfloat -> (GLfloat,GLfloat,GLfloat) -> [Char] -> [(GLfloat, GLfloat, GLfloat)]
generateLinesPoints edge lineLength startPoint formula = iter startPoint formula 0 []
    where
        iter _ [] _ result = result
        iter (sX,sY,sZ) formula alpha result
            | head formula == '+' = iter (sX,sY,sZ) (tail formula) (alpha + edge) result
            | head formula == '-' = iter (sX,sY,sZ) (tail formula) (alpha - edge) result
            | head formula == 'F' = iter newPoint (tail formula) alpha (result ++ [(sX,sY,sZ)] ++ [newPoint])
            | otherwise = iter (sX,sY,sZ) (tail formula) alpha result
             where
                    newPoint = (myRotate (sX,sY,sZ) (sX+lineLength,sY,sZ) alpha)

