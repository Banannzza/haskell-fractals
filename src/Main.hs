import Graphics.UI.GLUT
import Fractals
import System.Environment

 
-- поколение и длина линий
data Fractal = Snowflake String Int GLfloat | Dragon String Int GLfloat | Curve String Int GLfloat
    deriving (Show)


generateFractal :: Maybe Fractal -> [(GLfloat,GLfloat,GLfloat)]
generateFractal Nothing = []
generateFractal (Just (Snowflake aksioma p lineLength)) = do
    let formula = generate snowflakeRule aksioma p
    generateLinesPoints 60 lineLength (0,0,0) formula
generateFractal (Just (Dragon aksioma p lineLength)) = do
    let formula = generate dragonRule aksioma p
    generateLinesPoints 90 lineLength (0,0,0) formula
generateFractal (Just (Curve aksioma p lineLength)) = do
    let formula = generate curveRule aksioma p
    generateLinesPoints 60 lineLength (0,0,0) formula


parseArgs :: [String] -> Maybe Fractal
parseArgs [] = Nothing
parseArgs arguments
    | head arguments == "snowflake" = Just (Snowflake "F++F++F" (read $ arguments!!1 ::Int) (read $ arguments!!2 ::GLfloat))
    | head arguments == "dragon" = Just (Dragon "FX" (read $ arguments!!1 ::Int) (read $ arguments!!2 ::GLfloat))
    | head arguments == "curve" = Just (Curve "FL" (read $ arguments!!1 ::Int) (read $ arguments!!2 ::GLfloat))
    | otherwise = error "Bad args! look readme.md"



main :: IO ()
main = do
    args <- getArgs
    let mode = parseArgs args
    let myPoints = generateFractal mode
    (_progName, _args) <- getArgsAndInitialize
    _window <- createWindow "Fractal"  
    displayCallback $= display myPoints
    mainLoop
 
display :: [(GLfloat,GLfloat,GLfloat)] -> DisplayCallback
display  myPoints = do 
  clear [ColorBuffer]
  renderPrimitive Lines $
     mapM_ (\(x, y, z) -> vertex $ Vertex3 x y z) myPoints
  flush